## Физические размеры
Размеры платы должны быть кратны 10, 5, 2.5, 1 мм  
Платы должна иметь 4-2 крепежных отверстия под болты М3 или М2.5  
Растояние между креплений должны быть кратны 10, 5, 2.5, 1 мм  
Отсерстия под кремпление должы быть соеденены с полигомом земли  




## Надписи на плате
На плате должны быть подписаны разъёмы, желательно с их распиновкой  
Подписанно название платы с версией  
Тест поинты должны быть подписанны  
Если плата проектируеться под отпределенный PCB стек, плата должна быть подписанна этим стеком  
Все LED Должны быть функционально подписанны  


## Требования к схеме
Ручная проверка подключенности всех цепей питания  
В схеме должны быть нарисованна схема питания  
Желательно все линии должны быть подписанны (иметь NET_name)  
Желательно на каждой сигнальной линии иметь test point и подписать его на схеме  

## Проверки
Обязательно провести DRС контроль (PCB tools -> Design Rule Check -> Rules to check -> Run design rule check )  
Все переходные отверстия закрыты маской (тентированы)  

## Земля
Если слоев больше двух, один слой должен быть выделен полностью под питание  
Внешние слои должны быть залиты землей  
С краю платы должна быть площидка GND 10х5мм для подсоединения крокодила щупа осцилограффа  
Отсерстия под кремпление должы быть соеденены с полигомом земли  
Сшивка полигонов земли между собой via (PCB tools -> via stitching/shielding -> add via stitching to net )  


## PCB
Желательно на каждой сигнальной линии иметь test point и подписать его на схеме  
У каждой ножки питания должен быть кондер 0.1мкф  
GPIO и power line желатьльно иметь LED для контроля работы  
Все LED Должны быть функционально подписанны  
Все разъёмы должны быть не симетричные  
На плате не должно быть одинаковых, не совместимых разъёмов  


## СВЧ линии (> 1Гиг)
 Линии должныть быть открыты от маски (полигон на слое solder mask top\bottom)  
 Не иметь ломаных изгибов (все углы закругленны)  
 СВЧ разъмы дожны быть согласованны с линиями:  https://habr.com/ru/post/570492/  
 Периметр земли вокруг СВЧ линий должен быть прошит VIA (PCB tools->via stitching/shielding -> add  shielding to net )  
 Земля под СВЧ линиями должна быть не прерывна.  
 Если плата проектируеться под отпределенный PCB стек, плата должна быть подписанна этим стеком  
 
 
## Доп хотелки  
 Проект должен быть загружен в git  
 В проекте все компоненты должны иметь 3D Модели  
 В папке проекта должны быть:  
  PDF со схемой  (PCB file->smart pdf)
  step file С 3д моделью платы  (PCB file->export-> step 3D )  
  BOM-лист (список компонентов) для платы в .xls (PCB reports>Bill of materials)  
  Todo.txt  с требуемыми доработками платы  
  readme.md c коатким описание функционала платы  
 